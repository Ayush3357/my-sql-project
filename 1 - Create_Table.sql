#Drop Tables if exists before executing 
use messageexchange;
CREATE TABLE User (
  id varchar(250) NOT NULL,
  name varchar(50) NOT NULL,
  password varchar(50) Not Null,
  PRIMARY KEY (id) 
);

Create Table Circle(
   id varchar(250) NOT NULL,
   name varchar(50) NOT NULL,
   created_by varchar(250) not null,
   created_at datetime not null default now(),
   primary key(id),
   foreign key(created_by) references User(Id)
);

Create Table User_Circle(
	user_id varchar(250) not null,
    circle_id varchar(250) not null,
    isactive bit default 1,
    foreign key(user_id) references User(id),
    foreign key(circle_id) references Circle(id)
);

Create Table Messages(
	id int not null AUTO_INCREMENT,
    Subject varchar(2000) not null,
    Body Text not null,
    Sender varchar(250) not null,
    receiver varchar(250) not null,
    primary key(id)
);

Create Table User_inbox(
	id int not null AUTO_INCREMENT,
    message_id int not null,
    user_id varchar(250) not null,
    primary key(id),
    foreign key(message_id) references Messages(id)
);

#Trigger to validate receiver
delimiter $$
create trigger Validate_Receiver
Before insert
On Messages for each row
begin
	DECLARE usercount INT;
	DECLARE circlecount INT;
   select count(*) into usercount from User where id = new.receiver;
   select count(*) into circlecount from Circle where id = new.receiver;
   If usercount = 0 and circlecount = 0 THEN
		SIGNAL SQLSTATE '45000'
		SET MESSAGE_TEXT = 'Cannot add or update row: receiver not found';
	END IF;
End; $$

#Trigger to create inbox row after each insert in message
delimiter $$
CREATE TRIGGER Send_Message_To_Inbox
AFTER INSERT
ON Messages FOR EACH ROW
BEGIN
	DECLARE usercount INT;
	DECLARE circlecount INT;
   DECLARE cursor_List_isdone BOOLEAN DEFAULT FALSE;
   DECLARE cur_receiver varchar(250);

   DECLARE cursor_List CURSOR FOR 
      SELECT user_id from user_circle where circle_id = new.receiver and isactive = 1
    ;

   DECLARE CONTINUE HANDLER FOR NOT FOUND SET cursor_List_isdone = TRUE;
   select count(*) into usercount from User where id = new.receiver;
   select count(*) into circlecount from Circle where id = new.receiver;
   if usercount > 0 then
        INSERT INTO User_inbox(message_id, user_id)
        VALUES(new.id,new.receiver);
	elseif (circlecount > 0) then
		OPEN cursor_List;

		loop_List: LOOP
			FETCH cursor_List INTO cur_receiver;
				IF cursor_List_isdone THEN
					LEAVE loop_List;
				END IF;

			INSERT INTO User_inbox(message_id, user_id)
				VALUES(new.id, cur_receiver);
		END LOOP loop_List;
		CLOSE cursor_List;
   End If;
END; $$