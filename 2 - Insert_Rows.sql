use messageexchange;
#insert into user table
insert into user(id, name, password) values('sakshi.parmar@wipro.com', 'Sakshi Parmar', 'SkP@2020');
insert into user(id, name, password) values('varun.sharma@wipro.com', 'Varun Sharma', 'VaS@2020');
insert into user(id, name, password) values('rupal.gupta@wipro.com', 'Rupal Gupta', 'RuG@2020');
insert into user(id, name, password) values('utkrsh.shukla@wipro.com', 'Utkarsh Shukla', 'UtS@2020');
insert into user(id, name, password) values('neha.kumari@wipro.com', 'Neha Kumari', 'NeK@2020');
insert into user(id, name, password) values('shikha.thareja@wipro.com', 'Shikha Thareja', 'ShT@2020');
insert into user(id, name, password) values('ayushi.gupta@wipro.com', 'Ayushi Gupta', 'AyG@2020');
insert into user(id, name, password) values('karan.thapa@wipro.com', 'Karan Thapa', 'KaT@2020');
insert into user(id, name, password) values('deepak.porwal@wipro.com', 'Deepak porwal', 'DeP@2020');
insert into user(id, name, password) values('santosh.sreedhar@wipro.com', 'Santosh Sreedhar','SaS@2020');
insert into user(id, name, password) values('ravi.kumar@wipro.com', 'Ravi Kumar', 'RaK@2020');
insert into user(id, name, password) values('mohan.darla@wipro.com', 'Mohan Darla', 'MoD@2020');
insert into user(id, name, password) values('clinton.joshi@wipro.com', 'Clinton Joshi', 'ClJ@2020');
insert into user(id, name, password) values('yatin.chandna@wipro.com', 'Yatin Chandna', 'YtC@2020');

#insert into Circle table
insert into circle(id, name, created_by) values('wase_students@wipro.com', 'Wase Students', 'santosh.sreedhar@wipro.com');
insert into circle(id, name, created_by) values('wase_teachers@wipro.com', 'Wase Teachers', 'ravi.kumar@wipro.com');
insert into circle(id, name, created_by) values('other_organization@gmail.com', 'Other Organization', 'rupal.gupta@wipro.com');
insert into circle(id, name, created_by) values('non_wasians@wipro.com', 'Non Wasians', 'mohan.darla@wipro.com');
insert into circle(id, name, created_by) values('hdc@wipro.com', 'HDC Employees', 'clinton.joshi@wipro.com');
insert into circle(id, name, created_by) values('gndc@wipro.com', 'GNDC Employees', 'varun.sharma@wipro.com');

#insert into user_circle table
insert into user_circle(user_id, circle_id) values('sakshi.parmar@wipro.com','wase_students@wipro.com');
insert into user_circle(user_id, circle_id) values('varun.sharma@wipro.com','wase_students@wipro.com');
insert into user_circle(user_id, circle_id) values('utkrsh.shukla@wipro.com','wase_students@wipro.com');
insert into user_circle(user_id, circle_id) values('neha.kumari@wipro.com','wase_students@wipro.com');
insert into user_circle(user_id, circle_id) values('shikha.thareja@wipro.com','wase_students@wipro.com');
insert into user_circle(user_id, circle_id) values('deepak.porwal@wipro.com','wase_students@wipro.com');
insert into user_circle(user_id, circle_id) values('clinton.joshi@wipro.com','wase_students@wipro.com');
insert into user_circle(user_id, circle_id) values('ravi.kumar@wipro.com','wase_teachers@wipro.com');
insert into user_circle(user_id, circle_id) values('santosh.sreedhar@wipro.com','wase_teachers@wipro.com');
insert into user_circle(user_id, circle_id) values('sakshi.parmar@wipro.com','hdc@wipro.com');
insert into user_circle(user_id, circle_id) values('neha.kumari@wipro.com','hdc@wipro.com');
insert into user_circle(user_id, circle_id) values('shikha.thareja@wipro.com','hdc@wipro.com');
insert into user_circle(user_id, circle_id) values('clinton.joshi@wipro.com','hdc@wipro.com');
insert into user_circle(user_id, circle_id) values('varun.sharma@wipro.com','gndc@wipro.com');
insert into user_circle(user_id, circle_id) values('deepak.porwal@wipro.com','gndc@wipro.com');
insert into user_circle(user_id, circle_id) values('karan.thapa@wipro.com','gndc@wipro.com');
insert into user_circle(user_id, circle_id) values('utkrsh.shukla@wipro.com','gndc@wipro.com');
insert into user_circle(user_id, circle_id) values('ayushi.gupta@wipro.com','other_organization@gmail.com');
insert into user_circle(user_id, circle_id) values('rupal.gupta@wipro.com','other_organization@gmail.com');
insert into user_circle(user_id, circle_id) values('yatin.chandna@wipro.com','other_organization@gmail.com');
insert into user_circle(user_id, circle_id) values('ayushi.gupta@wipro.com','non_wasians@wipro.com');
insert into user_circle(user_id, circle_id) values('mohan.darla@wipro.com','non_wasians@wipro.com');
insert into user_circle(user_id, circle_id) values('yatin.chandna@wipro.com','non_wasians@wipro.com');

#Insert into Messages
insert into messages(Subject, Body, Sender, Receiver) values('Urgent Assignment', 'Hi Students, Complete assignment urgently', 'ravi.kumar@wipro.com', 'wase_students@wipro.com');
insert into messages(Subject, Body, Sender, Receiver) values('Complete SQL Training', 'Hi Sakshi, Please complete SQL Training', 'santosh.sreedhar@wipro.com', 'sakshi.parmar@wipro.com');
insert into messages(Subject, Body, Sender, Receiver) values('Good Morning', 'Hi Sakshi, Good Morning', 'yatin.chandna@wipro.com', 'sakshi.parmar@wipro.com');
insert into messages(Subject, Body, Sender, Receiver) values('Leaving Organization', 'Hi All, I am going to leave WIPRO soon', 'rupal.gupta@wipro.com', 'hdc@wipro.com');
insert into messages(Subject, Body, Sender, Receiver) values('Cab Cancelled', 'Hi All, Due to heavy rainfall, cab is cancelled today', 'varun.sharma@wipro.com', 'gndc@wipro.com');